#include "obstacle.h"

Obstacle::Obstacle(QVector3D _position, float _rayon)
    :position(_position), rayon(_rayon)
{
    unsigned stack_count = 10;
    unsigned slice_count = 10;
    nb_tri = (stack_count-1)*(slice_count-1)*2;

    GLfloat* vertices_obs = new GLfloat[nb_tri*3*3];
    std::vector<QVector3D> pts;

    for (unsigned j = 0 ; j < stack_count ; ++j)
    {
        for (unsigned i = 0 ; i < slice_count ; ++i)
        {
            float x, y, z;
            float theta  = -M_PI+(2*M_PI*j/(stack_count-1));
            float lambda = -M_PI/2+(M_PI*i/(slice_count-1)) ;
            x = rayon * cos(theta)*cos(lambda);
            y = rayon * sin(theta)*cos(lambda);
            z = rayon * sin(lambda);

            pts.push_back(QVector3D(x,y,z));
        }
    }

    for (unsigned j = 0 ; j < stack_count-1 ; ++j)
    {
        for (unsigned i = 0 ; i < slice_count-1 ; ++i)
        {
            unsigned c = (slice_count-1)*18;

            vertices_obs[j*c+i*18  ] = pts[j*slice_count+i][0];
            vertices_obs[j*c+i*18+1] = pts[j*slice_count+i][1];
            vertices_obs[j*c+i*18+2] = pts[j*slice_count+i][2];

            vertices_obs[j*c+i*18+3] = pts[j*slice_count+i+1][0];
            vertices_obs[j*c+i*18+4] = pts[j*slice_count+i+1][1];
            vertices_obs[j*c+i*18+5] = pts[j*slice_count+i+1][2];

            vertices_obs[j*c+i*18+6] = pts[(j+1)*slice_count+i+1][0];
            vertices_obs[j*c+i*18+7] = pts[(j+1)*slice_count+i+1][1];
            vertices_obs[j*c+i*18+8] = pts[(j+1)*slice_count+i+1][2];

            vertices_obs[j*c+i*18+9] = pts[(j+1)*slice_count+i+1][0];
            vertices_obs[j*c+i*18+10] = pts[(j+1)*slice_count+i+1][1];
            vertices_obs[j*c+i*18+11] = pts[(j+1)*slice_count+i+1][2];

            vertices_obs[j*c+i*18+12] = pts[(j+1)*slice_count+i][0];
            vertices_obs[j*c+i*18+13] = pts[(j+1)*slice_count+i][1];
            vertices_obs[j*c+i*18+14] = pts[(j+1)*slice_count+i][2];

            vertices_obs[j*c+i*18+15] = pts[j*slice_count+i][0];
            vertices_obs[j*c+i*18+16] = pts[j*slice_count+i][1];
            vertices_obs[j*c+i*18+17] = pts[j*slice_count+i][2];

        }
    }

    GLfloat texCoords_obs[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f
        };

    QVector<GLfloat> vertData_obs;
    for (int i = 0; i < nb_tri*9; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_obs.append(vertices_obs[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_obs.append(texCoords_obs[(i*2+j)%12]);
    }

    vbo_obs.create();
    vbo_obs.bind();
    vbo_obs.allocate(vertData_obs.constData(), vertData_obs.count() * int(sizeof(GLfloat)));
}

double Obstacle::getDistance(const QVector3D p)
{
    return sqrt(pow(p.x()-position.x(),2)+
                pow(p.y()-position.y(),2)+
                pow(p.z()-position.z(),2));
}

bool Obstacle::collisionDistance(const QVector3D p, float d)
{
    return (getDistance(p) <= d+rayon);
}

bool Obstacle::collision(const QVector3D p)
{
    return collisionDistance(p,0.0f);
}

void Obstacle::affiche(QOpenGLShaderProgram* sp)
{
    vbo_obs.bind();

    sp->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    sp->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    sp->enableAttributeArray("in_position");
    sp->enableAttributeArray("in_uv");

    modelMatrix.setToIdentity();
    modelMatrix.translate(position);
    sp->setUniformValue("modelMatrix", modelMatrix);

//    qDebug() << nb_tri;
    glDrawArrays(GL_TRIANGLES, 0, nb_tri*3);

    sp->disableAttributeArray("modelMatrix");
    sp->disableAttributeArray("in_position");
    sp->disableAttributeArray("in_uv");

    vbo_obs.release();
}
