#ifndef BANC_H
#define BANC_H

#include <vector>
#include "poisson.h"
#include "obstacle.h"
#include "predateur.h"
#include "boundingbox.h"

class Banc
{
private:
    std::vector<Poisson> poissons;
    QVector3D vitesse;
    std::vector<Obstacle> obstacles;
    std::vector<Predateur> predateurs;
    BoundingBox box;

    QOpenGLBuffer vbo_poisson;
    unsigned nb_tri;
    static const float MAX_FOR;

public:
    Banc(unsigned nb_poissons, BoundingBox b, QVector3D _vit = {0.0f,0.0f,0.0f});
    ~Banc();

    void anime(float dt);
    void affiche(QOpenGLShaderProgram* sp);
    void afficherPredateurs(QOpenGLShaderProgram* sp);
    void afficherBoite(QOpenGLShaderProgram* sp);
    void afficherObstacles(QOpenGLShaderProgram* sp);

    QVector3D separation (Poisson poisson);
    QVector3D alignement (Poisson poisson);
    QVector3D cohesion   (Poisson poisson);

    void resteDansBoite        (unsigned index);
    bool predateurMangePoisson (unsigned index);

    QVector3D getLimite(const unsigned c) const;
    std::vector<Poisson> getPoissons() const { return poissons; }

    void addPredateur(Predateur p) { predateurs.push_back(p); }
    void addObstacle (Obstacle  o) { obstacles.push_back(o);  }
    void setBoite(float largeur, float hauteur, float profondeur, QVector3D o={0,0,0});

};

#endif // BANC_H
