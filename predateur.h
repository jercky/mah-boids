#ifndef PREDATEUR_H
#define PREDATEUR_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <math.h>

#include "poisson.h"

class Predateur
{
private:
    QVector3D position;
    float rayon;
    static const float MAX_VIT;

    unsigned nb_tri;
    QOpenGLBuffer vbo_pre;
    QMatrix4x4 modelMatrix;
public:
    Predateur(QVector3D _position, float _rayon);
    void destroy() { vbo_pre.destroy(); }

    bool collision(const QVector3D position);
    bool collisionDistance(const QVector3D position, float distance);
    double getDistance(const QVector3D p);

    void anime(float dt, std::vector<Poisson> v_p);
    void affiche(QOpenGLShaderProgram* shaderprog);

    inline QVector3D getPosition() const { return position; }
    inline float getRayon() const { return rayon; }
    inline void grandir(const float aggrandissement) { rayon += aggrandissement; }
};

#endif // PREDATEUR_H
