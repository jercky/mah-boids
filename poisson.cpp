#include "poisson.h"

float Poisson::distance = 4.0f;
const float Poisson::MAX_VIT = 8.0f;

Poisson::Poisson(QVector3D _pos, float _taille, QVector3D _vit)
    : position(_pos), taille(_taille), vitesse(_vit), acceleration({0,0,0})
{

}

void Poisson::anime(float dt)
{
    position += vitesse*dt;
    acceleration = {0,0,0};
}

double Poisson::distance_avec_poisson(QVector3D p)
{
    return float(sqrt (pow(position[0]-p[0],2) + pow(position[1]-p[1],2) + pow(position[2]-p[2],2) ));
}

bool Poisson::dans_voisinage(QVector3D p)
{
    return (distance_avec_poisson(p) <= distance);
}

void Poisson::affiche(QOpenGLShaderProgram* sp, QOpenGLBuffer vbo_particle, unsigned nb_tri)
{
    vbo_particle.bind();
    sp->enableAttributeArray("in_position");
    sp->enableAttributeArray("in_uv");
    sp->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    sp->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));

    QMatrix4x4 modelMatrix;
    modelMatrix.setToIdentity();
    modelMatrix.translate(position);

    if(vitesse.x() != 0 || vitesse.z() != 0)
    {
        int signeAngle=1;
        if(vitesse.z() > 0) signeAngle=-1;
        float angleXZ=signeAngle *acos(vitesse.x()/sqrt(pow(vitesse.x(),2)+pow(vitesse.z(),2)))*180.0 / M_PI +90;
        modelMatrix.rotate(angleXZ,0,1,0);
    }
    if(vitesse.length() != 0)
    {
        float angleY=acos(vitesse.y()/vitesse.length())*180.0 / M_PI - 90.0;
        modelMatrix.rotate(angleY,1,0,0);
    }

    sp->setUniformValue("modelMatrix", modelMatrix);

    glDrawArrays(GL_TRIANGLES, 0, nb_tri*3);

    sp->disableAttributeArray("modelMatrix");
    sp->disableAttributeArray("in_position");
    sp->disableAttributeArray("in_uv");

    vbo_particle.release();
}


void Poisson::regulerVecteur (QVector3D* vec, const float limite)
{
    if ( (*vec).lengthSquared() > float(pow(limite,2)) )
    {
        (*vec).normalize();
        (*vec) *=limite;
    }
}

void Poisson::regulerVitesse ()
{
    regulerVecteur(&vitesse, MAX_VIT);
}

bool cmpf(float A, float B, float epsilon)
{
    return (fabs(A - B) < epsilon);
}

