#-------------------------------------------------
#
# Project created by QtCreator 2019-01-12T12:27:11
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tp01
TEMPLATE = app


SOURCES += main.cpp\
        banc.cpp \
        obstacle.cpp \
        poisson.cpp \
        princ.cpp \
        glarea.cpp \
    predateur.cpp \
    boundingbox.cpp

HEADERS  += princ.h \
        banc.h \
        glarea.h \
        obstacle.h \
        poisson.h \
    predateur.h \
    boundingbox.h

FORMS    += princ.ui

RESOURCES += \
    tp01.qrc
