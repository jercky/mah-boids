#include "banc.h"
#include <math.h>

const float Banc::MAX_FOR = 2.0f;

Banc::Banc(unsigned nb_poissons, BoundingBox b, QVector3D _vit)
    :vitesse(_vit), box(b)
{
    for (unsigned i = 0 ; i < nb_poissons ; ++i)
    {
        float x = (float(rand()%20000)/10000.0f-1.0f)*20;
        float y = (float(rand()%20000)/10000.0f-1.0f)*20;
        float z = (float(rand()%20000)/10000.0f-1.0f)*20;
        poissons.push_back(Poisson(QVector3D(x,y,z),1.0f,vitesse));
    }

    Poisson::setDistance(8.0f);

    nb_tri = 6;

    GLfloat vertices_poisson[] = {
        0.125f, 0.1f, 0.5f,
        0.0f  , 0.0f, 0.1f,
        0.125f, 0.1f, 0.0f,

        0.125f, 0.1f, 0.5f,
        0.25f  , 0.0f, 0.1f,
        0.125f, 0.1f, 0.0f,

        0.125f, 0.1f, 0.5f,
        0.0f  , 0.0f, 0.1f,
        0.25f  , 0.0f, 0.1f,

        0.0f  , 0.0f, 0.1f,
        0.125f, 0.1f, 0.0f,
        0.125f  , 0.05f,-0.05f,

        0.25f  , 0.0f, 0.1f,
        0.125f, 0.1f, 0.0f,
        0.125f  , 0.05f,-0.05f,

        0.125f  , 0.05f,-0.05f,
        0.25f  , 0.0f, 0.1f,
        0.0f  , 0.0f, 0.1f,
    };

    int scale = 3;
    for (int i = 0; i < nb_tri*3*3; ++i)
        vertices_poisson[i] *= scale;


    GLfloat couleur_poisson[] = {
            0.5f, 0.5f, 0.5f,
            0.9f, 0.9f, 0.9f,
            0.9f, 0.9f, 0.9f,

            0.5f, 0.5f, 0.5f,
            0.9f, 0.9f, 0.9f,
            0.9f, 0.9f, 0.9f,

            0.5f, 0.5f, 0.5f,
            0.9f, 0.9f, 0.9f,
            0.9f, 0.9f, 0.9f,

            0.5f, 0.5f, 0.5f,
            0.9f, 0.9f, 0.9f,
            0.9f, 0.9f, 0.9f,

            0.5f, 0.5f, 0.5f,
            0.9f, 0.9f, 0.9f,
            0.9f, 0.9f, 0.9f,

            1.0f, 0.25f, 0.25f,
            1.0f, 0.5f, 0.2f,
            1.0f, 0.5f, 0.2f,
        };

    QVector<GLfloat> vertData_poisson;
    for (int i = 0; i < nb_tri*3; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_poisson.append(vertices_poisson[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 3; j++)
            vertData_poisson.append(couleur_poisson[i*3+j]);
    }

    vbo_poisson.create();
    vbo_poisson.bind();
    vbo_poisson.allocate(vertData_poisson.constData(), vertData_poisson.count() * int(sizeof(GLfloat)));
}

Banc::~Banc()
{
    box.destroy();
    for (auto& o: obstacles)
        o.destroy();
    for (auto p : predateurs)
        p.destroy();
    vbo_poisson.destroy();
}

void Banc::setBoite(float largeur, float hauteur, float profondeur, QVector3D o)
{
    box = BoundingBox(largeur, hauteur, profondeur, o);
}


QVector3D Banc::separation(Poisson poisson)
{
    QVector3D temp = {0,0,0};
    double count = 0;

    // Gestion des poissons voisins
    for (auto p : poissons)
    {
        if (p.dans_voisinage(poisson.getPosition()) && p!=poisson)
        {
            QVector3D diff = poisson.getPosition()-p.getPosition();
            double coeff = poisson.distance_avec_poisson(p.getPosition());
            coeff += (coeff==0.0 ? 0.01 : 0.0);
            temp+= diff/coeff;
            count += 1/coeff;
        }
    }

    // Gestion des obstacles
    for (auto o : obstacles)
    {
        if (o.collisionDistance(poisson.getPosition(),poisson.getDistance()))
        {
            QVector3D diff = (poisson.getPosition()-o.getPosition());
            double coeff = o.getDistance(poisson.getPosition());
            coeff += (coeff==0.0 ? 0.01 : 0.0);
            temp += diff/coeff;
            count += 1/coeff;
        }
    }

    // Gestion des prédateurs
    for (auto p : predateurs)
    {
        if (p.collisionDistance(poisson.getPosition(),poisson.getDistance()*2))
        {
            QVector3D diff = (poisson.getPosition()-p.getPosition());
            double coeff = p.getDistance(poisson.getPosition())/poissons.size();
            coeff += (coeff==0.0 ? 0.01 : 0.0);
            temp += diff/coeff;
            count += 1/coeff;
        }
    }

    // Gestion de la proximité avec les bords de la boîte
    if (!box.distance_boite_acceptable(poisson.getPosition(),poisson.getDistance()))
    {
        QVector3D diff = -box.projection_boite(poisson.getPosition(), poisson.getDistance());
        double coeff = 1;
        coeff += (coeff==0.0 ? 0.01 : 0.0);
        temp += diff/coeff;
        count += 1/coeff;
    }

    if (temp.length() > 0)
        temp /= count;

    return temp;
}

QVector3D Banc::alignement(Poisson poisson)
{
    QVector3D temp = {0,0,0};
    unsigned count = 0;

    for (auto p : poissons)
    {
        if (poisson.dans_voisinage(p.getPosition()))
        {
            temp += p.getVitesse();
            count++;
        }
    }

    if (count > 0)
    {
        temp /= count;
        temp -= poisson.getVitesse();
    }

    return temp;
}

QVector3D Banc::cohesion(Poisson poisson)
{
    QVector3D temp = {0,0,0};
    unsigned count = 0;

    for (auto p : poissons)
    {
        if (poisson.dans_voisinage(p.getPosition()))
        {
            temp += p.getPosition();
            count++;
        }
    }
    if (count > 0)
    {
        temp /= count;
        temp -= poisson.getPosition();
    }

    return temp;
}

bool Banc::predateurMangePoisson (unsigned index)
{
    Poisson poisson = poissons[index];
    bool pasCollision = true;
    for (std::vector<Predateur>::iterator it = predateurs.begin(); it!= predateurs.end() && pasCollision ; ++it)
        if ((*it).collision(poisson.getPosition()))
        {
            poissons.erase(poissons.begin()+index);
            pasCollision = false;
            qDebug() << "Un poisson mangé ! Il en reste " << poissons.size() << " encore en vie.";
            (*it).grandir(0.02f);
        }
    return pasCollision;
}

void Banc::resteDansBoite(unsigned index)
{
    if (!box.est_dans_la_boite(poissons[index].getPosition()+poissons[index].getVitesse()))
        poissons[index].setVitesse(poissons[index].getVitesse()*box.reste_dans_la_boite(poissons[index].getPosition()+poissons[index].getVitesse())*2.0f);
}

void Banc::anime(float dt)
{
    // Calcul des nouvelles vitesses
    float L = 0.8f;
    std::vector<QVector3D> temp;
    for (auto& poisson : poissons)
    {
        QVector3D sep = separation(poisson);
        QVector3D ali = alignement(poisson);
        QVector3D coh =   cohesion(poisson);

        QVector3D v = 1.5f * sep + 1.0f * ali + 1.0f * coh;
        Poisson::regulerVecteur(&v,MAX_FOR);
        QVector3D new_vit = L * v + (1-L) * poisson.getVitesse();

        temp.push_back(new_vit);
    }

    // Anime les poissons
    for (unsigned i = 0 ; i < temp.size() ; ++i)
    {
        poissons[i].addAcceleration(temp[i]);
        bool pasCollision = predateurMangePoisson(i);

        if (pasCollision)
        {
            resteDansBoite(i);
            poissons[i].anime(dt);
        }
    }

    // Animation des prédateurs
    for (auto& p : predateurs)
        p.anime(dt,poissons);
}

QVector3D Banc::getLimite(const unsigned c) const
{
    return box.getLimite(c);
}

void Banc::afficherPredateurs(QOpenGLShaderProgram* sp)
{
    for (auto& p:predateurs)
        p.affiche(sp);
}

void Banc::afficherBoite(QOpenGLShaderProgram *sp)
{
    box.affiche(sp);
}

void Banc::afficherObstacles(QOpenGLShaderProgram *sp)
{
    for (auto& o:obstacles)
        o.affiche(sp);
}

void Banc::affiche(QOpenGLShaderProgram* sp)
{
    for (auto& poisson : poissons)
        poisson.affiche(sp, vbo_poisson, nb_tri*3);

}
