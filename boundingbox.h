#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include <QVector>
#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <math.h>

class BoundingBox
{
private:
    QVector3D limites[2];
    QOpenGLBuffer vbo_cube;
public:
    BoundingBox(QVector3D min, QVector3D max);
    BoundingBox(float largeur, float hauteur, float profondeur, QVector3D o = {0,0,0});
    void destroy() { vbo_cube.destroy(); }

    bool est_dans_la_boite(QVector3D new_pos);
    double distance_boite(QVector3D new_pos);
    bool distance_boite_acceptable(QVector3D new_pos, float distance);
    QVector3D reste_dans_la_boite(QVector3D new_pos);
    QVector3D projection_boite(QVector3D new_pos, float distance);
    QVector3D getLimite(const unsigned c) const;
    void affiche(QOpenGLShaderProgram* shaderprog);

    inline QVector3D at(const unsigned i) const { return limites[i]; }
    void setVBO();
};

const GLfloat cube[] = {
    //bas
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,

    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, 1.0f,

    1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, -1.0f,

    1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,

    //devant
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,

    1.0f, -1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,

    1.0f, 1.0f, -1.0f,
    -1.0f, 1.0f, -1.0f,

    -1.0f, 1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,

    //gauche
    -1.0f, -1.0f, -1.0f,
    -1.0f, 1.0f, -1.0f,

    -1.0f, 1.0f, -1.0f,
    -1.0f, 1.0f, 1.0f,

    -1.0f, 1.0f, 1.0f,
    -1.0f, -1.0f, 1.0f,

    -1.0f, -1.0f, 1.0f,
    -1.0f, -1.0f, -1.0f,

    //derrière
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,

    -1.0f, 1.0f, 1.0f,
    -1.0f, -1.0f, 1.0f,

    -1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, 1.0f,

    1.0f, -1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,

    //droite
    1.0f, 1.0f, 1.0f,
    1.0f, -1.0f, 1.0f,

    1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, -1.0f,

    1.0f, -1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,

    1.0f, 1.0f, -1.0f,
    1.0f, 1.0f, 1.0f,

    //haut
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,

    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, -1.0f,

    -1.0f, 1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,

    1.0f, 1.0f, -1.0f,
    -1.0f, 1.0f, -1.0f
};

#endif // BOUNDINGBOX_H
