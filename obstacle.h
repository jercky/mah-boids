#ifndef OBSTACLE_H
#define OBSTACLE_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <math.h>

class Obstacle
{
private:
    QVector3D position;
    float rayon;

    unsigned nb_tri;

    QOpenGLBuffer vbo_obs;
    QMatrix4x4 modelMatrix;
public:
    Obstacle(QVector3D _position, float _rayon);
    void destroy() { vbo_obs.destroy(); }

    void affiche(QOpenGLShaderProgram* shaderprog);
    inline QVector3D getPosition() const { return position; }
    inline float getRayon() const { return rayon; }

    bool collision(const QVector3D position);
    bool collisionDistance(const QVector3D position, float distance);

    double getDistance(const QVector3D p);
};

#endif // OBSTACLE_H
