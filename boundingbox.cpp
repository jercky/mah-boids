#include "boundingbox.h"

BoundingBox::BoundingBox(QVector3D min, QVector3D max)
{
    limites[0] = min;
    limites[1] = max;
    setVBO();
}

BoundingBox::BoundingBox(float largeur, float hauteur, float profondeur, QVector3D o)
{
    limites[0] = QVector3D(o[0]-largeur/2, o[1]-hauteur/2, o[2]-profondeur/2);
    limites[1] = QVector3D(o[0]+largeur/2, o[1]+hauteur/2, o[2]+profondeur/2);
    setVBO();
}

QVector3D BoundingBox::reste_dans_la_boite(QVector3D new_pos)
{
    return QVector3D((new_pos[0] >= limites[0][0] && new_pos[0] <= limites[1][0])*2-1,
                     (new_pos[1] >= limites[0][1] && new_pos[1] <= limites[1][1])*2-1,
                     (new_pos[2] >= limites[0][2] && new_pos[2] <= limites[1][2])*2-1);
}

QVector3D BoundingBox::getLimite(const unsigned c) const
{
    QVector3D m = {cube[c*3],
                   cube[c*3+1],
                   cube[c*3+2]};
    QVector3D r = {at(m[0]==1.0f)[0],
                   at(m[1]==1.0f)[1],
                   at(m[2]==1.0f)[2]};
    return r;
}

void BoundingBox::setVBO()
{
    GLfloat vertices_cube[48*3];
    for (unsigned i = 0 ; i < 48 ; ++i)
    {
        QVector3D point = getLimite(i);
        vertices_cube[i*3]   = point[0];
        vertices_cube[i*3+1] = point[1];
        vertices_cube[i*3+2] = point[2];
    }

    GLfloat texCoords_cube[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f
        };

    QVector<GLfloat> vertData_cube;
    for (int i = 0; i < 48; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_cube.append(vertices_cube[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 2; j++)
            vertData_cube.append(texCoords_cube[(i*2+j)%12]);
    }

    vbo_cube.create();
    vbo_cube.bind();
    vbo_cube.allocate(vertData_cube.constData(), vertData_cube.count() * int(sizeof(GLfloat)));

}

bool BoundingBox::est_dans_la_boite(QVector3D new_pos)
{
    return ((new_pos[0] >= limites[0][0] && new_pos[0] <= limites[1][0]) &&
            (new_pos[1] >= limites[0][1] && new_pos[1] <= limites[1][1]) &&
            (new_pos[2] >= limites[0][2] && new_pos[2] <= limites[1][2]));
}

double BoundingBox::distance_boite(QVector3D new_pos)
{
    QVector3D t = reste_dans_la_boite(new_pos);
    double d = 0.0;
    for (unsigned i = 0 ; i < 3 ; ++i)
    {
        if (t[i] <= 0.0f)
            d += pow(new_pos[i] - limites[i][0],2);
        else
            d += pow(new_pos[i] - limites[i][1],2);
    }

    return sqrt(d);
}

bool BoundingBox::distance_boite_acceptable(QVector3D new_pos, float distance)
{
    return ((new_pos[0] >= limites[0][0]+distance && new_pos[0] <= limites[1][0]-distance) &&
            (new_pos[1] >= limites[0][1]+distance && new_pos[1] <= limites[1][1]-distance) &&
            (new_pos[2] >= limites[0][2]+distance && new_pos[2] <= limites[1][2]-distance));
}

void BoundingBox::affiche(QOpenGLShaderProgram* sp)
{
    vbo_cube.bind();

    sp->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 5 * sizeof(GLfloat));
    sp->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 2, 5 * sizeof(GLfloat));
    sp->enableAttributeArray("in_position");
    sp->enableAttributeArray("in_uv");

    QMatrix4x4 modelMatrixCube;
    modelMatrixCube.setToIdentity();
    sp->setUniformValue("modelMatrix", modelMatrixCube);

    glDrawArrays(GL_LINES, 0, 48*3);

    sp->disableAttributeArray("modelMatrix");
    sp->disableAttributeArray("in_position");
    sp->disableAttributeArray("in_uv");
    vbo_cube.release();
}

QVector3D BoundingBox::projection_boite(QVector3D pos, float distance)
{
    QVector3D a = QVector3D(0,0,0);

    if (abs(pos[0]-limites[0][0]) < distance && abs(pos[0]-limites[1][0]) < distance)
        a[0] = 0;
    else if (abs(pos[0]-limites[0][0]) < abs(pos[0]-limites[1][0]))
        a[0] = limites[0][0] - pos[0];
    else
        a[0] = limites[1][0] - pos[0];

    if (abs(pos[1]-limites[0][1]) < distance && abs(pos[0]-limites[1][1]) < distance)
        a[1] = 0;
    else if (abs(pos[1]-limites[0][1]) < abs(pos[1]-limites[1][1]))
        a[1] = limites[0][1] - pos[1];
    else
        a[1] = limites[1][1] - pos[1];

    if (abs(pos[2]-limites[0][2]) < distance && abs(pos[0]-limites[1][2]) < distance)
        a[2] = 0;
    else if (abs(pos[2]-limites[0][2]) < abs(pos[2]-limites[1][2]))
        a[2] = limites[0][2] - pos[2];
    else
        a[2] = limites[1][2] - pos[2];

    return a;
}
