#ifndef POISSON_H
#define POISSON_H

#include <QVector3D>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <math.h>

bool cmpf(float A, float B, float epsilon = 0.005f);

class Poisson
{
private:
    static float distance;
    static const float MAX_VIT;
    QVector3D position;
    float taille;
    QVector3D vitesse;
    QVector3D acceleration;
public:
    Poisson(QVector3D _pos, float _taille, QVector3D _vit);

    double distance_avec_poisson (QVector3D p);
    bool dans_voisinage (QVector3D p);

    void anime(float dt);
    void affiche(QOpenGLShaderProgram* sp, QOpenGLBuffer vbo_particle, unsigned nb_tri);

    inline void translate (const QVector3D mov) { position += mov ; }
    inline void scale (const float scale)       { taille *= scale ; }

    void addAcceleration (const QVector3D toadd) { acceleration += toadd;
                                                   vitesse += acceleration;
                                                   regulerVitesse(); }
    static void regulerVecteur (QVector3D* vec, const float limite);
    void regulerVitesse ();

    bool operator==(const Poisson& p2) {return (cmpf(distance,p2.getDistance()) &&
                                                position == p2.getPosition() &&
                                                cmpf(taille, p2.taille) &&
                                                vitesse == p2.getVitesse());}

    bool operator!=(const Poisson& p2) {return !(*this == p2);}

    inline QVector3D getPosition() const        { return position ; }
    inline QVector3D getVitesse () const        { return vitesse  ; }
    inline float     getDistance () const       { return distance ; }
    inline QVector3D getAcceleration() const    { return acceleration ; }
    inline static float    getMaxVit()          { return MAX_VIT ; }

    inline static void setDistance (const float _dist) { distance = _dist; }
    inline void setVitesse  (const QVector3D v) { vitesse = v ;     }
    inline void setPosition (const QVector3D p) { position = p ;    }
};

#endif // POISSON_H
