// Basé sur :
// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>

const int NB_POISSON = 400;

GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);
    setFormat(sf);

    setEnabled(true);                   // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus);    // accepte focus
    setFocus();                         // donne le focus

    timer = new QTimer(this);
    timer->setInterval(20);           // msec
    connect (timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    timer->start();
    elapsedTimer.start();
}


GLArea::~GLArea()
{
    delete timer;

    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();
    tearGLObjects();
    doneCurrent();
}


void GLArea::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(0.2f,0.2f,0.4f,1.0f);
    glEnable(GL_DEPTH_TEST);

    makeGLObjects();

    // shader de cube
    program_cube = new QOpenGLShaderProgram(this);
    program_cube->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/simple.vsh");
    program_cube->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/simple.fsh");
    if (! program_cube->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_cube->log();
    }
    program_cube->setUniformValue("texture", 0);


    // shader de l'obstacle
    program_obs = new QOpenGLShaderProgram(this);
    program_obs->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/simple.vsh");
    program_obs->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/simple.fsh");
    if (! program_obs->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_obs->log();
    }
    program_obs->setUniformValue("texture", 0);


    // shader des poissons
    program_poisson = new QOpenGLShaderProgram(this);
    program_poisson->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/couleur.vsh");
    program_poisson->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/couleur.fsh");
    if (! program_poisson->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_poisson->log();
    }
    program_poisson->setUniformValue("texture", 0);


    // shader du prédateur
    program_pre = new QOpenGLShaderProgram(this);
    program_pre->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/couleur.vsh");
    program_pre->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/couleur.fsh");
    if (! program_pre->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << program_pre->log();
    }
    program_pre->setUniformValue("texture", 0);

}


void GLArea::makeGLObjects()
{
    BoundingBox box = BoundingBox(50,50,50);
    banc = new Banc(NB_POISSON,box);

//    obs.push_back(Obstacle({0,0,0}, 3));
    banc->addObstacle(Obstacle({25,25,25},1));
    banc->addObstacle(Obstacle({25,-25,25},1));
    banc->addObstacle(Obstacle({-25,25,25},1));
    banc->addObstacle(Obstacle({-25,-25,25},1));
    banc->addObstacle(Obstacle({25,25,-25},1));
    banc->addObstacle(Obstacle({25,-25,-25},1));
    banc->addObstacle(Obstacle({-25,25,-25},1));
    banc->addObstacle(Obstacle({-25,-25,-25},1));
    banc->addObstacle(Obstacle({0,0,-25}, 0.5));
    banc->addObstacle(Obstacle({0,0,25}, 0.5));
    banc->addObstacle(Obstacle({0,25,0}, 0.5));
    banc->addObstacle(Obstacle({0,-25,0}, 0.5));
    banc->addObstacle(Obstacle({25,0,0}, 0.5));
    banc->addObstacle(Obstacle({-25,0,0}, 0.5));
    banc->addObstacle(Obstacle({0,0,0}, 2));

    banc->addPredateur(Predateur({0,0,0},0.5));
    banc->addPredateur(Predateur({10,0,10},0.2));

    // Création de textures
    QImage image_sol(":/textures/ground.jpg");
    if (image_sol.isNull())
        qDebug() << "load image ground.jpg failed";
    textures[0] = new QOpenGLTexture(image_sol);

    QImage image_particule(":/textures/puff.png");
    if (image_particule.isNull())
        qDebug() << "load image puff.png failed";
    textures[1] = new QOpenGLTexture(image_particule);

    QImage image_cube(":/textures/cube.png");
    if (image_particule.isNull())
        qDebug() << "load image puff.png failed";
    textures[2] = new QOpenGLTexture(image_cube);
}


void GLArea::tearGLObjects()
{
    delete banc;
    for (int i = 0; i < 2; i++)
        delete textures[i];
}


void GLArea::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    windowRatio = float(w) / h;
}


void GLArea::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Matrice de projection
    QMatrix4x4 projectionMatrix;
    projectionMatrix.perspective(45.0f, windowRatio, 1.0f, 1000.0f);

    // Matrice de vue (caméra)
    QMatrix4x4 viewMatrix;
    viewMatrix.translate(xPos, yPos, zPos);
    viewMatrix.rotate(xRot, 1, 0, 0);
    viewMatrix.rotate(yRot, 0, 1, 0);
    viewMatrix.rotate(zRot, 0, 0, 1);

    // -- Affichage des poissons
    program_poisson->bind();
    program_poisson->setUniformValue("projectionMatrix", projectionMatrix);
    program_poisson->setUniformValue("viewMatrix", viewMatrix);

    banc->affiche(program_poisson);

    program_poisson->release();

    // -- Affichage du cube

    program_cube->bind();
    program_cube->setUniformValue("projectionMatrix", projectionMatrix);
    program_cube->setUniformValue("viewMatrix", viewMatrix);

    banc->afficherBoite(program_cube);

    program_cube->release();

    // -- Affichage d'un obstacle

    program_obs->bind();
    program_obs->setUniformValue("projectionMatrix", projectionMatrix);
    program_obs->setUniformValue("viewMatrix", viewMatrix);

    textures[0]->bind();
    banc->afficherObstacles(program_obs);
    textures[0]->release();

    program_obs->release();

    // -- Affichage des prédateurs

    program_pre->bind();
    program_pre->setUniformValue("projectionMatrix", projectionMatrix);
    program_pre->setUniformValue("viewMatrix", viewMatrix);

    banc->afficherPredateurs(program_pre);

    program_pre->release();
}


void GLArea::keyPressEvent(QKeyEvent *ev)
{
    float da = 1.0f;

    switch(ev->key()) {
        case Qt::Key_A :
            xRot -= da;
            update();
            break;

        case Qt::Key_Q :
            xRot += da;
            update();
            break;

        case Qt::Key_Z :
            yRot -= da;
            update();
            break;

        case Qt::Key_S :
            yRot += da;
            update();
            break;

        case Qt::Key_E :
            zRot -= da;
            update();
            break;

        case Qt::Key_D :
            zRot += da;
            update();
            break;
    }
}


void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}


void GLArea::mousePressEvent(QMouseEvent *ev)
{
    lastPos = ev->pos();
}


void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}


void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    int dx = ev->x() - lastPos.x();
    int dy = ev->y() - lastPos.y();

    if (ev->buttons() & Qt::LeftButton) {
        xRot += dy;
        yRot += dx;
        update();
    } else if (ev->buttons() & Qt::RightButton) {
        xPos += dx/10.0f;
        yPos -= dy/10.0f;
        update();
    } else if (ev->buttons() & Qt::MidButton) {
        xPos += dx/10.0f;
        zPos += dy;
        update();
    }

    lastPos = ev->pos();
}


void GLArea::onTimeout()
{
    static qint64 old_chrono = elapsedTimer.elapsed(); // static : initialisation la première fois et conserve la dernière valeur
    qint64 chrono = elapsedTimer.elapsed();
    dt = (chrono - old_chrono) / 1000.0f;
    banc->anime(dt);
    old_chrono = chrono;



    update();
}
