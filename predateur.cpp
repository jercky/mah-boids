#include "predateur.h"

const float Predateur::MAX_VIT = 2.0f;

Predateur::Predateur(QVector3D _position, float _rayon)
    :position(_position), rayon(_rayon)
{
    unsigned stack_count = 10;
    unsigned slice_count = 10;
    nb_tri = (stack_count-1)*(slice_count-1)*2;

    GLfloat* vertices_pre = new GLfloat[nb_tri*3*3];
    std::vector<QVector3D> pts;

    for (unsigned j = 0 ; j < stack_count ; ++j)
    {
        for (unsigned i = 0 ; i < slice_count ; ++i)
        {
            float x, y, z;
            float theta  = -M_PI+(2*M_PI*j/(stack_count-1));
            float lambda = -M_PI/2+(M_PI*i/(slice_count-1)) ;
            x = 1 * cos(theta)*cos(lambda);
            y = 1 * sin(theta)*cos(lambda);
            z = 1 * sin(lambda);

            pts.push_back(QVector3D(x,y,z));
        }
    }

    for (unsigned j = 0 ; j < stack_count-1 ; ++j)
    {
        for (unsigned i = 0 ; i < slice_count-1 ; ++i)
        {
            unsigned c = (slice_count-1)*18;

            vertices_pre[j*c+i*18  ] = pts[j*slice_count+i][0];
            vertices_pre[j*c+i*18+1] = pts[j*slice_count+i][1];
            vertices_pre[j*c+i*18+2] = pts[j*slice_count+i][2];

            vertices_pre[j*c+i*18+3] = pts[j*slice_count+i+1][0];
            vertices_pre[j*c+i*18+4] = pts[j*slice_count+i+1][1];
            vertices_pre[j*c+i*18+5] = pts[j*slice_count+i+1][2];

            vertices_pre[j*c+i*18+6] = pts[(j+1)*slice_count+i+1][0];
            vertices_pre[j*c+i*18+7] = pts[(j+1)*slice_count+i+1][1];
            vertices_pre[j*c+i*18+8] = pts[(j+1)*slice_count+i+1][2];

            vertices_pre[j*c+i*18+9] = pts[(j+1)*slice_count+i+1][0];
            vertices_pre[j*c+i*18+10] = pts[(j+1)*slice_count+i+1][1];
            vertices_pre[j*c+i*18+11] = pts[(j+1)*slice_count+i+1][2];

            vertices_pre[j*c+i*18+12] = pts[(j+1)*slice_count+i][0];
            vertices_pre[j*c+i*18+13] = pts[(j+1)*slice_count+i][1];
            vertices_pre[j*c+i*18+14] = pts[(j+1)*slice_count+i][2];

            vertices_pre[j*c+i*18+15] = pts[j*slice_count+i][0];
            vertices_pre[j*c+i*18+16] = pts[j*slice_count+i][1];
            vertices_pre[j*c+i*18+17] = pts[j*slice_count+i][2];

        }
    }

    GLfloat couleurs_pre[] = {
            1.0f, 0.0f, 0.0f
        };

    QVector<GLfloat> vertData_pre;
    for (int i = 0; i < nb_tri*9; ++i) {
        // coordonnées sommets
        for (int j = 0; j < 3; j++)
            vertData_pre.append(vertices_pre[i*3+j]);
        // coordonnées texture
        for (int j = 0; j < 3; j++)
            vertData_pre.append(couleurs_pre[j]);
    }

    vbo_pre.create();
    vbo_pre.bind();
    vbo_pre.allocate(vertData_pre.constData(), vertData_pre.count() * int(sizeof(GLfloat)));
}

void Predateur::anime(float dt, std::vector<Poisson> v_p)
{
    if (!v_p.empty())
    {
        Poisson destination = v_p.at(0);
        double min_dist = destination.distance_avec_poisson(position);
        for (auto& p: v_p)
        {
            if (min_dist > p.distance_avec_poisson(position))
            {
                destination = p;
                min_dist = p.distance_avec_poisson(position);
            }
        }

        QVector3D new_vit = destination.getPosition()-position;
        new_vit.normalize();
        new_vit *= MAX_VIT;
        position += new_vit*dt;
    }
}

void Predateur::affiche(QOpenGLShaderProgram* sp)
{
    vbo_pre.bind();

    sp->setAttributeBuffer("in_position", GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    sp->setAttributeBuffer("in_uv", GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
    sp->enableAttributeArray("in_position");
    sp->enableAttributeArray("in_uv");

    modelMatrix.setToIdentity();
    modelMatrix.translate(position);
    modelMatrix.scale(rayon);
    sp->setUniformValue("modelMatrix", modelMatrix);

    glDrawArrays(GL_TRIANGLES, 0, nb_tri*3);

    sp->disableAttributeArray("modelMatrix");
    sp->disableAttributeArray("in_position");
    sp->disableAttributeArray("in_uv");

    vbo_pre.release();
}

bool Predateur::collision(const QVector3D p)
{
    return collisionDistance(p,0.0f);
}

bool Predateur::collisionDistance(const QVector3D p, float d)
{
    return (getDistance(p) <= d+rayon);
}

double Predateur::getDistance(const QVector3D p)
{
    return sqrt(pow(p.x()-position.x(),2)+
                pow(p.y()-position.y(),2)+
                pow(p.z()-position.z(),2));
}
